package com.ADSI1836648.Bike.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ClientWithSaleDTO {
    @NotNull
    int id;

    private LocalDateTime dateSale;
    @NotNull
    int clientId;
    @NotNull
    int bikeId;
    String name;
    String documentNumber;
    String phoneNumber;
    String serial;
    String model;
    Double price;
    String email;
    Boolean status;
}
