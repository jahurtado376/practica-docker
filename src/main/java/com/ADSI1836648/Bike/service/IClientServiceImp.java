package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Client;
import com.ADSI1836648.Bike.domain.Sale;
import com.ADSI1836648.Bike.repository.ClientRepository;
import com.ADSI1836648.Bike.service.dto.ClientWithSaleDTO;
import com.ADSI1836648.Bike.service.dto.SaleDTO;
import com.ADSI1836648.Bike.service.transformer.SaleTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IClientServiceImp  implements IClientService{

    @Autowired
    ClientRepository clientRepository;

    @Override
    public Iterable<Client> read(String document, String email) {
        if(document != null){
            return  clientRepository.findAllByDocumentNumberEquals(document);
        }
        if(email != null){
            return clientRepository.findByEmailEquals(email);
        }
            return clientRepository.findAll();
    }

    public Client findClientByDocumentContains(String document) {
            return clientRepository.findByDocumentNumberEquals(document);

    }

    public List<Sale> findClientByDocumentWithSale(String document) {
        return  clientRepository.findClientByDocumentWithSale(document)
                .stream().map(SaleTransformer::getSaleFromClientWithSaleDTO).collect(Collectors.toList());
    }
    @Override
    public Client update(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public ResponseEntity<Client> create(Client client) {
        if (clientRepository.findByDocumentNumber(client.getDocumentNumber()).isPresent()){
            return new ResponseEntity(client, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(clientRepository.save(client), HttpStatus.OK);
        }
    }

    @Override
    public void delete(Integer id) {
        clientRepository.deleteById(id);
    }

    @Override
    public Iterable<Client> search(String documentNumber) {
        return clientRepository.findByDocumentNumberContaining(documentNumber);
    }
}

